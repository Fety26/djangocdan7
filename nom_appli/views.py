from django.http import HttpResponse
from django.template import loader
from django.shortcuts import render

def affiche(request,nom):
    return HttpResponse('Hello '+ nom)

def getDetail(request,nom):
    template1 = loader.get_template('appli.html')
    
    context = {
        'title':'Hello python',
        'nom':nom
    }
    return HttpResponse(template1.render(context, request))


def detail(request):
    return render(request, 'enfant.html')