from django.urls import path

from nom_appli import views

urlpatterns = [
    path('affiche/<str:nom>/',views.affiche),
    path('getdetail/<str:nom>/',views.getDetail),
    path('detail/',views.detail),
]