from django.apps import AppConfig


class NomAppliConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'nom_appli'
