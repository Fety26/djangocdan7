
from django.contrib import admin
from django.urls import path, include
from nom_app import views
from django.contrib.auth import views as auth_views
from django.shortcuts import redirect
from django.urls import reverse_lazy

class CustomLogoutView(auth_views.LogoutView):
     def get(self, request, *args, **kwargs):
        next_page = reverse_lazy('login')  # Remplacez par le nom de l'URL de connexion
        return redirect(next_page)
    
urlpatterns = [
    path('login/', auth_views.LoginView.as_view(next_page=views.detail_personne), name='login'),
    path('logout/', CustomLogoutView.as_view(), name='logout'),
    path('register/', views.register, name='register'),
    path('admin/', admin.site.urls),
    path('app/',views.hello),
    path('personne/',views.detail_personne),
    path('appli/',include('nom_appli.url'))
]
