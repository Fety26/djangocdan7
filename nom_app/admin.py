from django.contrib import admin
from nom_app.models import Personne
# Si sans personalisation
#admin.site.register(Personne)

# Si avec personnalisation
@admin.register(Personne)
class PersonneAdmin(admin.ModelAdmin):
    list_display = ('nom', 'age')  # Champs à afficher dans la liste
    list_filter = ('nom',)  # Filtres disponibles dans la liste
    search_fields = ('nom', 'age')  # Champs de recherche