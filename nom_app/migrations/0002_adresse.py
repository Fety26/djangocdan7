# Generated by Django 4.2.7 on 2023-12-07 07:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('nom_app', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Adresse',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('rue', models.CharField(max_length=50)),
                ('code_postale', models.IntegerField()),
            ],
        ),
    ]
