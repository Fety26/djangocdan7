from django.http import HttpResponse
from nom_app.models import Personne,Adresse
from django.shortcuts import render
from nom_app.forms import PersonneForms
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm

@login_required
def hello(request):
    Adresse.objects.all()
    return HttpResponse("Hello everyone")

def detail_personne(request):
    if request.method =='POST':
        # Utilisation du form
        ajout = PersonneForms(request.POST)

        # Validation
        if ajout.is_valid():
            # Accéder aux données
            nom = ajout.cleaned_data.get('nom')
            age = ajout.cleaned_data.get('age')

            # Enregistrement
            Personne.objects.create(nom=nom,age=age)
        # Vider la forme    
        ajout = PersonneForms()

    else :
        ajout = PersonneForms()

    # Récuperation
    personnes = Personne.objects.all()
    return render(request, 'app.html',{'personnes':personnes,'form':ajout})

def register(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password1')
            user = authenticate(username = username, password = password)
            login(request, user)
            return redirect('login')
    else:
        form = UserCreationForm()
    return render(request, 'registration/register.html', {'form': form})
