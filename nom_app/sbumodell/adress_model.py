from django.db import models

class Adresse(models.Model):
    rue = models.CharField(max_length=50)
    code_postale = models.IntegerField()